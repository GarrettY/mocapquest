﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponDisplayManager : MonoBehaviour
{
    private GameObject Gun;
    public movementController player;
    [SerializeField]//sprite to represent AK
    private Sprite AKWeaponSprite;
    [SerializeField]//sprite to represent Shotgun
    private Sprite ShotgunWeaponSprite;
    private Image MyImage;
    public Sprite WeaponSprite;
    // Start is called before the first frame update
    void Start()
    {
        MyImage = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        //grabs the gun prefab that is in equiped weapon and sees what tag it has on it 
        Gun = player.equippedWeapon;
        if (player.equippedWeapon.tag == ("AK47"))
        {
            MyImage.sprite = AKWeaponSprite;

        }
        else if (player.equippedWeapon.tag == ("Shotgun"))
        {
            MyImage.sprite = ShotgunWeaponSprite;

        }

    }
}
