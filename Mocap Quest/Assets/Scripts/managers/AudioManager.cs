﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class AudioManager : MonoBehaviour
{
    [SerializeField, Tooltip("The master audio mixer")]
    private AudioMixer Master;
    [SerializeField, Tooltip("The slider value vs decibel volume curve")]
    private AnimationCurve volumeVsDecibels;
    public Slider MasterSlider;
    public Slider musicSlider;
    public Slider SFXSlider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {//set the sliders to the respective values
        Master.SetFloat("MasterVolume", volumeVsDecibels.Evaluate(MasterSlider.value));
        Master.SetFloat("BGM", volumeVsDecibels.Evaluate(musicSlider.value));
        Master.SetFloat("SFXSound", volumeVsDecibels.Evaluate(SFXSlider.value));
    }
}
