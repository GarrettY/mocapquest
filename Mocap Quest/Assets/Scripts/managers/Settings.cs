﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Settings : MonoBehaviour
{
    public int ScreenRes;
    public int qualityRes;
    public bool fullscreen;
    public Toggle FullScreenToggle;
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Resolution[] res;
    // Start is called before the first frame update
    void Start()
    {
        res = Screen.resolutions;
        foreach(Resolution resolution in res)
        {
            resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
  
    }
  public void OnScreenToggle()
    {
        // toggle fullscreen
        Screen.fullScreen = FullScreenToggle.isOn;
     
    }

    public void OnResolutionChange()
    {// Build resolutions
        Screen.SetResolution(res[resolutionDropdown.value].width, res[resolutionDropdown.value].height,Screen.fullScreen);

    }

    public void OnTextureChange()
    {// Build textures
        QualitySettings.masterTextureLimit = qualityRes = qualityDropdown.value;

    }
}
