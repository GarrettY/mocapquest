﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public int Livesremaining;
    // Start is called before the first frame update
    void Start()
    {
        Livesremaining = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (Livesremaining <= 0)
        {
            // send to death screen
            SceneManager.LoadScene("LoseScreen");
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            Livesremaining = Livesremaining - 1;
            Debug.Log("should lose a life");
        }
    }
}
