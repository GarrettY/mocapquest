﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Header("Camera target")]
    [SerializeField, Tooltip("The object this is following.")]
    public Transform playerTransform;

    [Header ("Camera Positioning")]
    public float cameraMoveSpeed;
    public Vector3 cameraPosition;
    // makes sure the camera is focused on the player all the time
    /// <summary>
    /// This one line of code controls the camera making sure that it follows the target 
    /// it is assigned to.
    /// </summary>
    void Update()
    {
        this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, playerTransform.position + cameraPosition, cameraMoveSpeed * Time.deltaTime);
    }
}
