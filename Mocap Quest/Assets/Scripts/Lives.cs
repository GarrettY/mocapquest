﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Lives : MonoBehaviour
{
    public GameManager lives;
    private Text text;
    private int LivesLeft;
    private void Awake()
    {
        text = GetComponent<Text>();
    }

   void Update()
    {
        //essentially the same for the health script
        LivesLeft = lives.Livesremaining;
        text.text = string.Format("Lives: {0}", LivesLeft);

    }

    void TestDie()
    {

    }
}
