﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    private AudioSource Sounds;
    [SerializeField]
    private GameObject teleEnd;
    // game object where the player will end up
    void Start()
    {
        Sounds = GetComponent<AudioSource>();
    }

    // teleports player to the teleend game object
    private void OnTriggerEnter(Collider other)
    {
        Sounds.Play();
        other.transform.position = teleEnd.transform.position;
    }
}
