﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField]
    private Canvas Menu;
   private AudioSource Sounds;
    private bool paused=false;
    // Start is called before the first frame update
    void Start()
    {
        Sounds = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //push down the pause button to pause
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (paused == false)
            {//time is stopped
                Time.timeScale = 0.0f;
                paused = true;
                //Menu is brought up
                Menu.enabled = true;
            }


        }
    }
    //used for the button 
    public void Unpause()
    {
         if (paused == true)
        {
            Sounds.Play();
            //resumes time
            Time.timeScale = 1.0f;
            //bool pause is false now
            paused = false;
            //make menu disappear
            Menu.enabled = false;
        }

    }

    public void QuitGame()
    {


    }

}
