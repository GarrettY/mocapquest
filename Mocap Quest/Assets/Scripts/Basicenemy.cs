﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Basicenemy : MonoBehaviour
{
    private Transform tf;
    int i;
    // public float speed;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Transform playertf;
    [SerializeField]
    private NavMeshAgent agent;

    public Animator anim;

    public float normalMoveSpeed;

    public float sprintSpeed;

    private Vector3 input;
    private void Awake()
    {
        Vector3 input = agent.desiredVelocity;
        input = transform.InverseTransformDirection(input);
        anim.SetFloat("Horizontal", input.x);
        anim.SetFloat("Vertical", input.z);
        player = GameObject.FindGameObjectWithTag("Player");
        playertf = player.transform;
    }
    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        //agent.speed = normalMoveSpeed;
        anim = GetComponent<Animator>();
        //player = GameObject.FindGameObjectWithTag("player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {//sets agent velocity to animation velocity
        agent.velocity = anim.velocity;
        //sets the enemy to persue to player
        agent.SetDestination(playertf.position);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ActivateRagdoll();
            Debug.Log("should ragdoll");
        }
    }

    void ActivateRagdoll()
    {

        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for (i = 0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = true;
        }

        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (i = 0; i < childColliders.Length; i++)
        {
            childColliders[i].enabled = true;
        }

        anim.enabled = false;
        agent.SetDestination(tf.position);
    }
}
