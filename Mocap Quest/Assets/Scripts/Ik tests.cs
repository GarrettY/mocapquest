﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Iktests : MonoBehaviour
{

    public Transform RHandtoPoint;
    public Transform LHandtoPoint;
    [SerializeField]
    private Animator anim;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    public float weight;

    void Start()
    {

    }

    protected virtual void OnAnimatorIK()
    {
        if (Vector3.Angle(transform.forward, RHandtoPoint.position - transform.position) > 90)
        {
            //rotate the body
            //set weight to 0
            anim.SetLookAtWeight(0);
        }
        else
        {
            anim.SetLookAtPosition(RHandtoPoint.position);
            anim.SetLookAtWeight(1.0f);
        }
        anim.SetIKPosition(AvatarIKGoal.RightHand, RHandtoPoint.position);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, weight);

        anim.SetIKRotation(AvatarIKGoal.RightHand, RHandtoPoint.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, weight);

        anim.SetIKPosition(AvatarIKGoal.LeftHand, LHandtoPoint.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, weight);

        anim.SetIKRotation(AvatarIKGoal.LeftHand, LHandtoPoint.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, weight);

    }
}
