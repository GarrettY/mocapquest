﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    private GameObject player;
    private void OnTriggerEnter(Collider other)
    {
        //finds if the thing bumped into is player via tag
        if(other.gameObject.tag == "Player")
        {
            player = other.gameObject;
            OnPickup(player);
        }
        
    }

    protected virtual void OnPickup(GameObject player)
    {//deletes pickup
        Destroy(gameObject);

    }
}
