﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : Pickup
{
    private AudioSource Sounds;
    [SerializeField] 
    private float healing;
    private GameObject player;
    void Start()
    {
        Sounds = GetComponent<AudioSource>();

    }
    // Start is called before the first frame update
    protected virtual void OnTriggerEnter(Collider other)
    {
        //find if the thing that bumped is player via tag search
        if (other.gameObject.tag == "Player")
        {
            Sounds.Play();
            player = other.gameObject;
            OnPickup(player);
        }
    }

    protected override void OnPickup(GameObject player)
    {
        
        //activates what is supposed to be going on in the health version of this
        Health tempHealth = player.GetComponent<Health>();
        //Invoke.onHeal(healing);
        //calls the base of original
        base.OnPickup(player);

    }
}
