﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
public class Enemy : MonoBehaviour
{
    [SerializeField]
    public Transform RHandtoPoint;
    [SerializeField]
    public Transform LHandtoPoint;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    public float weight;
    [SerializeField]
    private GameObject equippedWeapon;
    [SerializeField]
    private GameObject gunPrefab;
    [SerializeField]
    private Transform attachmentPoint;
    // public float speed;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Transform playertf;
    [SerializeField]
    private NavMeshAgent agent;
    
    public Animator anim;

    public float normalMoveSpeed;

    public float sprintSpeed;

    private Vector3 input;
    private void Awake()
    {
        Vector3 input = agent.desiredVelocity;
        input = transform.InverseTransformDirection(input);
        anim.SetFloat("Horizontal", input.x);
        anim.SetFloat("Vertical", input.z);
        player = GameObject.FindGameObjectWithTag("Player");
        playertf = player.transform;
        EquipWeapon();
    }
    // Use this for initialization
    void Start()
    {
        //agent.speed = normalMoveSpeed;
        anim = GetComponent<Animator>();
        //player = GameObject.FindGameObjectWithTag("player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
       agent.velocity = anim.velocity;
        agent.SetDestination(playertf.position);
    }

    public void EquipWeapon()
    {
        //equips the weapon that it is set to at the beginning 
        equippedWeapon = Instantiate(gunPrefab) as GameObject;
        //attaches it to the weapon container
        equippedWeapon.transform.SetParent(attachmentPoint);
        equippedWeapon.transform.localPosition = gunPrefab.transform.localPosition;
        equippedWeapon.transform.localRotation = gunPrefab.transform.localRotation;
        RHandtoPoint = equippedWeapon.transform.Find("right Hand yellow");
        LHandtoPoint = equippedWeapon.transform.Find("Left hand orange");

    }

    protected virtual void OnAnimatorIK()
    {

        //sets IK position to right hand position
        anim.SetIKPosition(AvatarIKGoal.RightHand, RHandtoPoint.position);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
        //sets IK rotation to right hand rotation
        anim.SetIKRotation(AvatarIKGoal.RightHand, RHandtoPoint.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);


        //does the same as above but with the left hand
        anim.SetIKPosition(AvatarIKGoal.LeftHand, LHandtoPoint.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);

        anim.SetIKRotation(AvatarIKGoal.LeftHand, LHandtoPoint.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);

    }
}
