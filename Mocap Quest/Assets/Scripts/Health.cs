﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Health : MonoBehaviour
{
    [SerializeField]
    private float Maxhealth;
    [SerializeField]
    private float currentHealth;
    public float Percent;
    [Header("Events")]
    [SerializeField, Tooltip("increasing health every time the player is healed")]
    private UnityEvent onHeal;
    [SerializeField, Tooltip("increasing health every time the player is damaged")]
    private UnityEvent onDamage;
    [SerializeField, Tooltip("increasing health every time the player is killed")]
    private UnityEvent onDeath;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        //calculates the percent of health remaining will put this in an activated scipt later
        Percent = currentHealth / Maxhealth;
    }

    

}
