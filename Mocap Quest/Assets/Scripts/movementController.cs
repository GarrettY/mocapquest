﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementController : MonoBehaviour
{
    [ SerializeField ]
    public Transform RHandtoPoint;
    [SerializeField]
    public Transform LHandtoPoint;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    public float weight;
    [SerializeField]
    public GameObject equippedWeapon;
    [SerializeField]
    private GameObject gunPrefab;
    [SerializeField]
    private Transform attachmentPoint;
    public Animator anim;
    public float moveSpeed = 5.0f;
    public Plane groundPlane;
    private GameObject player;
    private Transform tf;
    public float distanceToPlane;

    public Health health;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
        player = this.gameObject;
        EquipWeapon();
        health = GetComponent<Health>();
    }

    private void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //moveDirection = transform.InverseTransformDirection(moveDirection);
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1);
        moveDirection = moveDirection * moveSpeed;
        anim.SetFloat("Vertical", moveDirection.z);
        anim.SetFloat("Horizontal", moveDirection.x);

        RotatetoMouse();
    }

    public void RotatetoMouse()
    {
        //define ground Plane
        groundPlane = new Plane(Vector3.up, player.transform.position);
        //raycast through the mouse position to the ground plane
        float distanceToPlane;
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        groundPlane.Raycast(mouseRay, out distanceToPlane);
        //find a point on the plane
        Vector3 PointonPlane = mouseRay.GetPoint(distanceToPlane);
        //rotate towards that point

        //temp:change this to use"rotate towards"
        player.transform.LookAt(PointonPlane);
    }


    public void EquipWeapon()
    {
        //equips the weapon that it is set to at the beginning 
        equippedWeapon = Instantiate(gunPrefab) as GameObject;
        //attaches it to the weapon container
        equippedWeapon.transform.SetParent(attachmentPoint);
        equippedWeapon.transform.localPosition = gunPrefab.transform.localPosition;
        equippedWeapon.transform.localRotation = gunPrefab.transform.localRotation;
        RHandtoPoint = equippedWeapon.transform.Find("right Hand yellow");
        LHandtoPoint = equippedWeapon.transform.Find("Left hand orange");
        
    }

    protected virtual void OnAnimatorIK()
    {
            
            //sets IK position to right hand position
            anim.SetIKPosition(AvatarIKGoal.RightHand, RHandtoPoint.position);
           anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
        //sets IK rotation to right hand rotation
            anim.SetIKRotation(AvatarIKGoal.RightHand, RHandtoPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);


        //does the same as above but with the left hand
        anim.SetIKPosition(AvatarIKGoal.LeftHand, LHandtoPoint.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
        
        anim.SetIKRotation(AvatarIKGoal.LeftHand, LHandtoPoint.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);

    }
}