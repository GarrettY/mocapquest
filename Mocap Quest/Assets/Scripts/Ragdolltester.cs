﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Ragdolltester : MonoBehaviour
{
    int i;
    private Animator anim;
    private NavMeshAgent agent;


    // Start is called before the first frame update
    void Start()
    {
        anim=GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ActivateRagdoll();
            Debug.Log("should ragdoll");
        }
    }


    void ActivateRagdoll()
    {

        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for (i = 0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = true;
        }

        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (i = 0; i < childColliders.Length; i++)
        {
            childColliders[i].enabled = true;
        }

        anim.enabled = false;
        agent.enabled = false;
    }




}
